package main

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
)

type AwsConfig struct{}

func (c AwsConfig) New() (*AwsProvider, error) {
	sess, err := session.NewSession()
	if err != nil {
		return nil, err
	}

	return &AwsProvider{
		svc: sns.New(sess),
	}, nil
}

type AwsProvider struct {
	svc *sns.SNS
}

func (b AwsProvider) Send(msg *SMSRequest) error {
	req, _ := b.svc.PublishRequest(&sns.PublishInput{
		Message: aws.String(msg.Text),
		MessageAttributes: map[string]*sns.MessageAttributeValue{
			"Key": {
				DataType:    aws.String("String"),
				StringValue: aws.String("String"),
			},
		},
		PhoneNumber: aws.String(msg.To),
	})

	err := req.Send()

	if err != nil {
		return err
	}

	return nil
}
