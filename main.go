package main

import (
	"log"
	"net/http"
	"os"

	"github.com/labstack/echo"
)

func sendSMS(payload *SMSRequest) {
	config := AwsConfig{}
	provider, _ := config.New()
	err := provider.Send(payload)
	if err != nil {
		log.Println("[ERR]", payload.To, "->", payload.Text)
		log.Println(err)
	} else {
		log.Println("[OK]", payload.To, "->", payload.Text)
	}
}

type SMSRequest struct {
	User     string `json:"user" form:"user" query:"user"`
	Password string `json:"password" form:"password" query:"password"`
	To       string `json:"to" form:"to" query:"to"`
	Text     string `json:"text" form:"text" query:"text"`
}

type WebhookServer struct {
}

func (w *WebhookServer) webhook(c echo.Context) error {
	smsReq := new(SMSRequest)
	if err := c.Bind(smsReq); err != nil {
		log.Println("Invalid Request")
		return c.String(http.StatusInternalServerError, "INVALID REQUEST")
	}
	if smsReq.User == os.Getenv("SMS_USER") && smsReq.Password == os.Getenv("SMS_PASSWORD") {
		go sendSMS(smsReq)
		return c.String(http.StatusOK, "OK")
	} else {
		log.Println("User and password incorrect!")
		return c.String(http.StatusInternalServerError, "ERROR CREDENTIAL")
	}
}

func (w *WebhookServer) RunForever() {
	e := echo.New()
	e.HideBanner = true
	if os.Getenv("SMS_USER") == "" || os.Getenv("SMS_PASSWORD") == "" || os.Getenv("AWS_REGION") == "" || os.Getenv("AWS_ACCESS_KEY_ID") == "" || os.Getenv("AWS_SECRET_ACCESS_KEY") == "" {
		log.Fatal("Environment variable `SMS_USER`, `SMS_PASSWORD`, `AWS_REGION`, `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` are all required!")
	}

	log.Println("SMS server started at path http://[IP address]/sms/send")
	log.Println("Usage: [HTTP GET] -> http://[IP Address]:[port]/sms/send?user=testuser&password=testpassword&to=8618612345678&text=this+is+a+test+message")

	e.GET("/sms/send", w.webhook)
	log.Fatal(e.Start("0.0.0.0:80"))
}

func main() {
	w := WebhookServer{}
	w.RunForever()
}
